

# 树莓派开机准备

首先，开箱后树莓派3B+或者4B，原装是没什么配件的，只是一个主控板和说明书，如下：

![](https://gd2.alicdn.com/imgextra/i2/63891318/O1CN01h2vidm1LbgazZrYE5-63891318.jpg)



# 树莓派3B+和4B的主要区别

![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01e5c3GM1Lbgat0g5YW-63891318.png)



# 开机需要自己准备的东西

1. 准备一张microSD卡，就是普通手机里面的那种SD卡

2. 准备一条数据线：如果3B+版本用micro usb数据线【就是安卓手机最早的那种数据线】；如果4B版本用Type-C接口的数据线【现在最新的安卓手机数据线】

# 官方开机教程

https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up

## 系统镜像文件下载链接

https://www.raspberrypi.org/downloads/

## SD卡格式化工具

https://www.sdcard.org/downloads/formatter/index.html

# 知识点：NOOBS和Raspbian的区别

https://www.cnblogs.com/visionsl/p/8117265.html

# 真正开始开机

## 方法1，NOOBS安装：

直接使用USB鼠标键盘和HDMI显示器，用NOOBS安装，这个很简单，基本上都是下一步。

## 方法2，Raspbian安装：

适合没有HDMI显示器，鼠标键盘和网线的

- 把SD卡通过度开启插入电脑，使用 SDCardFormatterv5_WinEN.zip 这个软件对SD卡进行格式化，本软件的安装文件可以在本节的文件夹下找到
  ![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01HOBh0w1LbgbIeiLn8_!!63891318.png)

- 使用 Win32DiskImager 软件安装Raspbian系统，本软件的安装文件可以在本节的文件夹下找到，记住这里是用解压出来的img文件，这个安装过程比较慢，耐心等待
  ![](https://img.alicdn.com/imgextra/i3/63891318/O1CN0181AsbQ1LbgbIJ89gt_!!63891318.png)

- 成功后**千万不要**按照系统提示进行格式化SD卡的盘，以后插入SD卡也不要按照提示格式化SD卡的盘。如果要重新烧录镜像，再用SDFormatter进行格式化
  ![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01BKnrns1LbgbMfUN0B_!!63891318.png)

- 安装成功后变为2个盘符，一个是boot，一个是打不开的【不要格式化！！！！】
  ![](https://img.alicdn.com/imgextra/i4/63891318/O1CN01Xl2o7k1LbgbHMJwqk_!!63891318.png)

- 在boot盘新建一个 wpa_supplicant.conf 文件，内容如下，其中这个conf文件里面直接改好自己的wifi的ssid（用户名）和psk（密码），可以让树莓派直接能通过这个文件连接到你的路由器，或者直接复制教程目录内的文件，自己改好ssid和密码后复制粘贴到boot盘

  ```
  country=CN
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1
  
  network={
         ssid="PUSHPULL2.4G"
         psk="qianqianzhao"
         key_mgmt=WPA-PSK
         priority=1
  }
  ```

- 并新建一个ssh文件，记住是文件不是文件夹，而且没有扩展名，这个ssh文件可以让树莓派开启SSH服务，可以使电脑通过ssh服务去连接树莓派，这也是我们能用电脑远程去操作的关键

- 把烧录好镜像文件的SD卡插入树莓派上【断电插】

- 用DC5V电源适配器通过usb数据线给树莓派供电
  供电红灯常亮： 未能检测到TF卡；
  双灯常亮： 未能检测到系统；
  黄灯闪烁： 系统运行正常。

- 看到黄灯闪烁后，打开自己的路由器列表，看下是否有新的网络加入，记录下它的IP![](https://img.alicdn.com/imgextra/i4/63891318/O1CN01isLSuL1LbgbOgzO7f_!!63891318.png)
  我的树莓派是192.168.0.114这个IP地址，自己的是多少就记录下来

- 打开putty或者其它的可以ssh的软件
  ![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01GYpROV1LbgbMhMF1m_!!63891318.png)

- 输入ip地址，成功的话会让你输入用户名（pi）和密码（raspberry）
  ![](https://img.alicdn.com/imgextra/i4/63891318/O1CN019BhQE71LbgbO2wkx5_!!63891318.png)

- 登陆成功，输入ls，能看到文件列表，说明已经连接树莓派成功。大功告成！！


# 用windows远程桌面连接树莓派，xrdp

需要依次输入以下命令

- 安装xrdp

  ```
  sudo apt-get install xrdp
  ```

- 安装VNC服务

  ```
  sudo apt-get install tightvncserver
  ```

- 启动xrdp

  ```
  sudo /etc/init.d/xrdp start
  ```

- 将Xrdp服务添加到系统默认启动服务中

  ```
  sudo update-rc.d xrdp defaults
  ```


- 全部安装成功![](https://img.alicdn.com/imgextra/i4/63891318/O1CN01M7LQ2C1LbgbCj79u5_!!63891318.png)

- 打开windows远程桌面
  ![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01juK62K1LbgbOhw8Bu_!!63891318.png)

- 输入用户名和密码进行登陆。树莓派默认用户名：pi，默认密码：raspberry

- 登陆成功，可以把树莓派放在角落里远程操作啦
![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01KBJQlw1LbgbMVCvF6_!!63891318.png)
  
- 一般树莓派开机要启动几十秒，这期间不要直接断电，会降低树莓派的使用寿命，对了,想要关闭树莓派，关机指令是

  ```
  sudo shutdown -h now
  ```
  
  






